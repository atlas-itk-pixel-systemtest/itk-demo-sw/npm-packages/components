import React from 'react'
import {
  TextInput,
  ValidatedOptions
} from '@patternfly/react-core'
import PropTypes from 'prop-types'

/**
 * The CheckedTextInput component provides a text field to be used for the
 * input of a single string.
 *
 * When no input is given, the field shows the 'name'-prop, to tell the user
 * which input value is required.
 * When no input is given, the field is marked with a danger symbol (or a
 * warning symbol if the input is optional). After entering a string value,
 * the field is marked with a success symbol.
 *
 * Props:
 * {
 *  value: string - Value to be shown in the field
 *  name: string - Name of the field. Shown if 'value' is undefined
 *  handleChange: function - Called when text field content changes
 *  optional: boolean - Empty field marked by warning (instead of error) symbol
 *  width: string - Width of field given as 'XXXpx' (default: '125px')
 * }
 */

const CheckedTextInput = (props) => {
  return (
    <TextInput
      aria-label={props.name + ' input'}
      type='text'
      validated={
        (props.value === '') || (props.value === undefined)
          ? (
            props.optional
              ? ValidatedOptions.warning
              : ValidatedOptions.error
          )
          : ValidatedOptions.success
      }
      onChange={props.handleChange}
      value={props.value !== undefined ? props.value : props.name}
      style={{
        width: props.width ? props.width : '125px',
        color: (props.value === '') || (props.value === undefined)
          ? 'rgb(0,0,0,0.5)'
          : 'black'
      }}
      onFocus={() => {
        if (props.value === undefined) props.handleChange('')
      }}
      onBlur={() => {
        if (props.value === '') props.handleChange(undefined)
      }}
    />
  )
}

CheckedTextInput.propTypes = {
  name: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  optional: PropTypes.bool,
  width: PropTypes.string
}

export default CheckedTextInput
