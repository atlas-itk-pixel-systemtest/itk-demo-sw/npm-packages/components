import React from 'react'
import {
  Alert,
  AlertActionCloseButton,
  AlertActionLink,
  AlertGroup
} from '@patternfly/react-core'
import PropTypes from 'prop-types'

/**
 * Notifications returns a list of Patternfly Alerts to be added to the top
 * of your application.
 *
 * By default, alerts dissapear after 8 seconds. They should be given in the
 * following format. Options marked with * are always required, options marked
 * with # are required only for errors.
 * {
 *  variant*: string - Log level ('danger', 'warning', 'info' and 'success')
 *  title*: string - Title of the message/ error
 *  timeout: boolean - If true, alert disappears automatically
 *  status#: integer - HTTP statuscode (Only for Errors!)
 *  message#: { (Only for Errors!)
 *    main#: string - Detailed Error message
 *    type: string - Optional URI with further information on the Error
 *  }
 * }
 *
 * NOTE: The useNotifications Hook provides a list of alerts in the above
 * format, based on RCF7807 Error responses. Its use with this component
 * is strongly suggested.
 *
 * Props: {
 *  alerts: array - List of alerts in the above format
 *  deleteAlert: function - Called, when an alert times out or is closed
 * }
 */

export const Notifications = (props) => {
  const renderedAlerts = props.alerts.map((log, i) => {
    if (!log) return null
    return log.variant === 'danger'
      ? (
        <Alert
          variant={log.variant}
          title={`${log.status} - ${log.title}`}
          timeout={log.timeout}
          isInline
          onTimeout={() => props.deleteAlert(i)}
          actionClose={
            <AlertActionCloseButton
              onClose={() => props.deleteAlert(i)}
            />
          }
          actionLinks={
            log.message.type
              ? (
                <AlertActionLink onClick={() => {
                  window.open(log.message.type, '_blank').focus()
                }}
                >
                  More information
                </AlertActionLink>
              )
              : null
          }
          key={`Alert_${i}`}
        >
          {log.message.main}
          {/* {
          log.message.instance
          ? (<p>{`Occurence in: ${log.message.instance}`}</p>)
          : null
          } */}
        </Alert>
      )
      : (
        <Alert
          variant={log.variant}
          title={log.title}
          timeout={log.timeout}
          // isInline
          onTimeout={() => props.deleteAlert(i)}
          actionClose={
            <AlertActionCloseButton
              onClose={() => props.deleteAlert(i)}
            />
          }
          key={`Alert_${i}`}
        />
      )
  })

  return (
    <AlertGroup isToast isLiveRegion>
      {renderedAlerts}
    </AlertGroup>
  )
}

Notifications.propTypes = {
  alerts: PropTypes.array.isRequired,
  deleteAlert: PropTypes.func.isRequired
}

export default Notifications
