import React, { useEffect, useState } from 'react'
import {
  Page,
  PageSection,
  PageSectionVariants,
  Flex,
  Title,
  Grid,
  GridItem
} from '@patternfly/react-core'
import Card from '../Card'
import PropTypes from 'prop-types'

/**
 * A Dashboard giving an overview over available services. Services must be provided in a Object containing an
 * array (named 'cards') consisting of the services. The Dashboard will display each service in a card.
 *
 * Each service should be an object with the following structure (required keys marked with *):
 * {
 *  id*: string - unique identifier used for selection. If one selection should select multiple services they
 *               can share an id.
 *  name*: string - name of the service.
 *  category*: string - the category in which the service is sorted.
 *  protocol = 'http': string - protocoll of the communication used to construct the url to the service
 *  host: string - hostname used to construct the url to the service. If no hostname is given, the url on which the
 *                 dashboard runs will be used.
 *  port: string - the port on which the service runs.
 *  health: string - health endpoint of the service. If provided regular requests will be made to monitor the status
 *                   of the service. Requires 'port' to function.
 *  icon: string - path or url to an icon for the service.
 *  description: string - a short description of the service.
 *  url: string - url to the service.
 *  urlPrefix = '': string - url extension to the backend ui. Only visible for advanced user
 * }
 *
 * Props (required marked with *):
 * {
 *  json*: object -  Object containing the services
 *  host*: string - Standard url to wich request will be send, if a service has no defined host
 *  advanced*: boolean - States if advanced infos should be shown
 *  title: string - Optional title. Will replace Dashboard.
 *  keyDisplayNames: object - Maps extra keys of a service to nice displaynames in the card ,
 *  perPage: number - Sets the target number of Cards per page in smallScreen mode,
 *  importantTypes: array - An array of strings used to sort the card types. The last one will be displayed first,
 * }
 */

const CardPanel = (props) => {
  const [cards, setCards] = useState(false)
  const [maxCardsLength, setMaxCardsLength] = useState(0)
  const [selectedItems, setSelected] = useState([])
  const minCardWidth = 300
  const [maxPerTopicPage, setMaxPerTopicPage] = useState(3)
  const minCardHeight = 180
  const [maxPerPage, setMaxPerPage] = useState(props.perPage ? props.perPage : 4)
  const importantTypes = ['Tools', 'Microservice-UI', 'Microservice']
  const MAXHUE = 320
  const MINHUE = 130

  const loadJSON = (data, sortBy) => {
    const newCards = {}
    if ('cards' in data) {
      data.cards.forEach((card) => {
        if (!(card[sortBy] in newCards)) newCards[card[sortBy]] = []
        newCards[card[sortBy]].push(card)
      })
    }
    const newMaxLength = Object.values(newCards).reduce((max, cards) => {
      const l = cards.length
      return l > max ? l : max
    }, 0)
    setCards(newCards)
    setMaxCardsLength(newMaxLength)
  }

  useEffect(() => {
    const width = Math.floor(window.innerWidth / minCardWidth - 1)
    const height = Math.floor(window.innerHeight / minCardHeight - 2.4)
    setMaxPerTopicPage(width < 1 ? 1 : width)
    setMaxPerPage(height < 1 ? 1 : height)
    window.addEventListener('resize', () => {
      const width = Math.floor(window.innerWidth / minCardWidth - 1)
      const height = Math.floor(window.innerHeight / minCardHeight - 2.4)
      setMaxPerTopicPage(width < 1 ? 1 : width)
      setMaxPerPage(height < 1 ? 1 : height)
    })
  }, [])

  useEffect(() => {
    loadJSON(props.json, props.sortBy)
  }, [props.json, props.sortBy])

  const onClick = (id) => {
    let newSelected = []
    if (!(selectedItems.includes(id))) newSelected = [...selectedItems, id]
    else newSelected = selectedItems.filter(x => x !== id)
    setSelected(newSelected)
  }

  const sortCards = (cards) => {
    const sorted = Object.entries(cards).sort((a, b) => {
      const imp = props.importantTypes ? props.importantTypes : importantTypes
      return imp.indexOf(b[0]) - imp.indexOf(a[0])
    })
    console.log(sorted)
    if (props.dense) {
      const dense = sorted.map(([cat, cs], i) => cs.map(c => [cat, c, i])).flat()
      console.log(dense)
      return dense
    }
    return sorted
  }

  const categorieAmount = Object.keys(cards).length
  const cardSection = !props.dense
    ? (
      <>
        <style>
          {
            `[data-hover="categoryBox"]:hover {
                  background-color: rgba(230, 230, 230, 0.2);
                  border-color: rgb(210, 210, 210);
                }`
          }
        </style>
        <Flex
          style={{
            flexDirection: 'column',
            flexWrap: 'nowrap',
            overflowY: 'auto',
            overflowX: 'hidden',
            height: '100%',
            width: '100%',
            gap: '10px'
          }}
        >
          {cards && sortCards(cards).map(([type, cards], i) => {
            let defaultUrlExtension = ''
            if (type === 'Microservice') {
              defaultUrlExtension = '/ui'
            }
            return (
              <Flex
                data-hover='categoryBox'
                key={type}
                style={{
                  border: '1px solid #ececec',
                  borderRadius: '5px',
                  paddingLeft: '10px',
                  paddingRight: '10px',
                  paddingTop: '5px',
                  width: 'calc(100% - 40px)'
                }}
              >
                <Title
                  headingLevel='h1'
                  style={{ marginBottom: '20px', textAlign: 'center', width: '100%' }}
                >
                  {type}
                </Title>
                <Grid style={{ overflowY: 'hidden', height: 'min-content', width: '100%', paddingBottom: '10px' }} hasGutter key={type}>
                  {cards.map((service) => {
                    console.log('CALCING GRIDWITH: ', maxPerTopicPage, maxCardsLength, Math.floor(12 / Math.min(maxPerTopicPage, maxCardsLength)))
                    return (
                      <GridItem key={service.id} span={Math.floor(12 / maxPerTopicPage)} rowSpan={Math.floor(12 / maxPerPage)}>
                        <Card
                          key={service.id}
                          selected={selectedItems.includes(service.id)}
                          service={service}
                          host={props.host}
                          onClick={onClick}
                          advanced={props.advanced}
                          keyDisplayNames={props.keyDisplayNames ? props.keyDisplayNames : {}}
                          heightFraction={1 / maxPerPage}
                          keysToShow={props.keysToShowInCards}
                          urlExtension={service.url_postfix || defaultUrlExtension}
                        />
                      </GridItem>)
                  }
                  )}
                </Grid>
              </Flex>)
          })}
        </Flex>
      </>)
    : (
      <>
        <Flex
          style={{
            flexDirection: 'column',
            flexWrap: 'nowrap',
            overflowY: 'auto',
            overflowX: 'hidden',
            height: '100%',
            width: '100%',
            gap: '10px'
          }}
        >
          <style>
            {
              `[data-hover="categoryBox"]:hover {
                  background-color: rgba(230, 230, 230, 0.2);
                  border-color: rgb(210, 210, 210);
                }`
            }
          </style>
          <Grid
            style={{
              height: 'min-content',
              width: 'calc(100% - 40px)',
              paddingTop: '10px'
            }} hasGutter
          >
            {cards && sortCards(cards).map(([type, service, i]) => {
              let defaultUrlExtension = ''
              const hue = MAXHUE - (MAXHUE - MINHUE) / categorieAmount * i
              const color = `hsl(${hue}deg 100% 70%)`
              if (type === 'Microservice') {
                defaultUrlExtension = '/ui'
              }
              return (
                <GridItem key={service.id} span={Math.floor(12 / maxPerTopicPage)} rowSpan={Math.floor(12 / maxPerPage)}>
                  <Card
                    key={service.id}
                    selected={selectedItems.includes(service.id)}
                    service={service}
                    host={props.host}
                    onClick={onClick}
                    advanced={props.advanced}
                    keyDisplayNames={props.keyDisplayNames ? props.keyDisplayNames : {}}
                    heightFraction={1 / maxPerPage}
                    keysToShow={props.keysToShowInCards}
                    urlExtension={service.url_postfix || defaultUrlExtension}
                    color={color}
                  />
                </GridItem>
              )
            })}
          </Grid>
        </Flex>
      </>)

  return (
    <Page style={{ overflowY: 'hidden', height: 'calc(100vh - 10rem - 80px)' }}>
      <PageSection
        isFilled
        variant={PageSectionVariants.light}
        style={{ overflowY: 'hidden', height: '100%' }}
      >
        <PageSection
          isFilled
          style={{ height: '100%' }}
        >
          <Flex
            style={{ height: '100%', flexWrap: 'nowrap' }}
            direction={{ default: 'row', sm: 'row', md: 'row', lg: 'row', xl: 'row' }}
          >
            {cardSection}
          </Flex>
        </PageSection>
      </PageSection>
    </Page>
  )
}

CardPanel.propTypes = {
  //   onInfo: PropTypes.func.isRequired,
  //   onWarn: PropTypes.func.isRequired,
  //   onError: PropTypes.func.isRequired,
  host: PropTypes.string.isRequired,
  json: PropTypes.object.isRequired,
  sortBy: PropTypes.string.isRequired,
  keyDisplayNames: PropTypes.object,
  keysToShowInCards: PropTypes.array,
  advanced: PropTypes.bool,
  perPage: PropTypes.number,
  importantTypes: PropTypes.array,
  dense: PropTypes.bool
}

export default CardPanel
