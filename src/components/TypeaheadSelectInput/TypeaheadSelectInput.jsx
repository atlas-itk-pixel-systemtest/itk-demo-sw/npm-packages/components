import React from 'react'
import {
  Select,
  SelectOption,
  SelectVariant
} from '@patternfly/react-core'
import PropTypes from 'prop-types'

/**
 * The TypeaheadSelectInput is a multifunctional component to be used for
 * selecting an input value from a list of given options.
 *
 * By default, typing in the Select field will filter the list of options. An
 * option can be selected by clicking on it. If no option is clicked, the
 * field will default to the placeholder text.
 * Optionally, the input value can be persistent, meaning any text typed
 * is kept as a selection, regardless of the available select-options.
 * The TypeaheadSelectInput's validation can be disabled via the noValidation-
 * prop. The optional-prop can turn the 'danger' symbol into a 'warning', if
 * the input is not validated.
 *
 * Props (Required props are marked with *):
 * {
 *   clearSelection*: function - Clears current selection
 *   handleSelect*: function - Called when a selectable option is clicked
 *   handleToggle*: function - Toggles between opening and closing select
 *   isOpen*: boolean - Is select open?
 *   selected*: string - Currently selected option
 *   selectOptions*: array - List of selectable options
 *   handleTypeaheadInputChanged: function - Called when typing in select field
 *   isDisabled: boolean - Disables Select field
 *   isInputValuePersisted: boolean - Enable persistent typing
 *   maxheight: maximum height of expanded select (default: '600%')
 *   noValidate: boolean - Disable validation UI elementts
 *   optional: boolean - Unvalidated field marked with 'warning' (not 'danger')
 *   placeholderText: string - Text shown for not selection ('Select an option')
 *   width: string - Width of the component (default: '100%')
 * }
 */

const TypeaheadSelectInput = (props) => {
  const handleClear = props.clearSelection
  const customFilter = (_, value) => {
    const options = props.selectOptions.map(
      (opt, i) => (<SelectOption key={i} value={opt} />)
    )
    if (!value) {
      return options
    }
    return options
    // const input = new RegExp(value, 'i')
    // return options.filter(child => input.test(child.props.value))
  }

  const validated = (
    props.noValidate
      ? 'default'
      : (props.selected !== null
        ? 'success'
        : (props.optional
          ? 'warning'
          : 'error'
        )
      )
  )

  return (
    <Select
      variant={SelectVariant.typeahead}
      selections={props.selected}
      isOpen={props.isOpen}
      isDisabled={props.isDisabled}
      isInputValuePersisted={props.isInputValuePersisted}
      onClear={handleClear}
      onFilter={customFilter}
      onSelect={props.onSelect}
      onToggle={props.onToggle}
      onTypeaheadInputChanged={props.handleTypeaheadInputChanged || null}
      placeholderText={props.placeholderText || 'Select an option'}
      maxHeight={props.maxheight || '600%'}
      width={props.width || '100%'}
      validated={validated}
    >
      {props.selectOptions.map(
        (opt, i) => (<SelectOption key={i} value={opt} />)
      )}

    </Select>
  )
}

TypeaheadSelectInput.propTypes = {
  clearSelection: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  selected: PropTypes.string,
  selectOptions: PropTypes.array.isRequired,
  handleTypeaheadInputChanged: PropTypes.func,
  isDisabled: PropTypes.bool,
  isInputValuePersisted: PropTypes.bool,
  maxheight: PropTypes.string,
  noValidate: PropTypes.bool,
  onSelect: PropTypes.func.isRequired,
  onToggle: PropTypes.func.isRequired,
  optional: PropTypes.bool,
  placeholderText: PropTypes.string,
  width: PropTypes.string
}

export default TypeaheadSelectInput
