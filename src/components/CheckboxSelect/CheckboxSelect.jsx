import React from 'react'
import {
  Select,
  SelectVariant,
  SelectOption
} from '@patternfly/react-core'
import PropTypes from 'prop-types'

/**
 * CheckboxSelect provides a Select component with multiple selectable
 * options.
 *
 * Props:
 * {
 *  isOpen: boolean - Is Select component opened?
 *  onToggle: function - Called when Select component is toggled
 *  onSelect: function - Called, when an option is selected
 *  options: array - List of selectable options
 *  selected: array - List of currently selected options
 *  type: string - Default value to be shown when no options are selected
 * }
 */

const CheckboxSelect = (props) => {
  const selectOptions = Array.from(props.options, (option) => (
    <SelectOption value={option} key={option} />
  ))

  return (
    <Select
      variant={SelectVariant.checkbox}
      aria-label={props.name}
      onToggle={props.onToggle}
      onSelect={props.onSelect}
      selections={props.selections}
      isOpen={props.isOpen}
      placeholderText={props.name}
    >
      {selectOptions}
    </Select>
  )
}

CheckboxSelect.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onToggle: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired,
  selections: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired
}

export default CheckboxSelect
