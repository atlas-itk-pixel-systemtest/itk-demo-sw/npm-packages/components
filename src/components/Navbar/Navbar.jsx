import React from 'react'
import {
  Nav,
  NavItem,
  NavList
} from '@patternfly/react-core'
import PropTypes from 'prop-types'

/**
 * Navbar returns a general Patternfly Nav for a given set of Navbar Items.
 *
 * Stateful logic of a Navbar can be found in the useNavbar Hook.
 *
 * Props: {
 *  itemNames: array - List of names of Navbar items
 *  activeItem: integer - Index of the currently active item
 *  changePanel: function - Called when a Navbar item is clicked
 * }
 */

const Navbar = (props) => {
  const handleSelect = props.changePanel
  const navItems = props.itemNames.map((item, i) =>
    <NavItem
      key={i}
      itemId={i}
      isActive={props.activeItem === i}
    >
      {item}
    </NavItem>
  )

  const pageNav = (
    <Nav onSelect={handleSelect} aria-label='Nav' variant='tertiary'>
      <NavList>
        {navItems}
      </NavList>
    </Nav>
  )

  return pageNav
}

Navbar.propTypes = {
  itemNames: PropTypes.array.isRequired,
  activeItem: PropTypes.number.isRequired,
  changePanel: PropTypes.func.isRequired
}

export default Navbar
