import React from 'react'
import PropTypes from 'prop-types'

/**
 * LoggingViewer returns a simple table of log messages, with corresponding
 * log-levels.
 *
 * Supported log-levels are error, warning and info. Individual log messages
 * are numbered. The indiviual log messages need to be in the following format:
 * {
 *  lvl: string - log level of the message
 *  msg: string - actual message
 * }
 *
 * Props: {
 *  content*: array - list of log objects in the above format
 *  height: string - height in the format 'XXXpx' (optional, default '250px')
 * }
 */

const LoggingViewer = (props) => {
  // Styles

  const numberStyle = {
    fontFamily: 'Courier New',
    textAlign: 'right',
    verticalAlign: 'top',
    borderRight: '1px solid #dddddd',
    paddingRight: '10px',
    width: '50px',
    color: '#6f6f6f'
  }

  const globalLvlStyles = {
    fontFamily: 'Courier New',
    textAlign: 'left',
    paddingRight: '15px',
    paddingLeft: '25px',
    verticalAlign: 'top',
    width: '90px'
  }
  /* eslint-disable */
  const lvlStyles = {
    info: {
      ...globalLvlStyles,
      color: 'blue'
    },
    warning: {
      ...globalLvlStyles,
      color: ' #bba31b'
    },
    error: {
      ...globalLvlStyles,
      color: 'red'
    }
  }
  /* eslint-enable */
  const globalTxtStyles = {
    fontFamily: 'Courier New',
    paddingLeft: '15px',
    color: '#000000'
  }
  const txtStyles = {
    info: {
      ...globalTxtStyles
    },
    warning: {
      ...globalTxtStyles
    },
    error: {
      ...globalTxtStyles
    }
  }

  const lvlNames = {
    info: 'Info',
    warning: 'Warning',
    error: 'Error'
  }

  const tableStyle = {
    backgroundColor: '#FFFFFF',
    borderCollapse: 'collapse',
    width: '100%',
    overflow: 'auto'
  }

  const backgroundStyle = {
    backgroundColor: '#FFFFFF',
    width: '100%',
    height: props.height ? props.height : '250px',
    border: '1px solid #dddddd',
    overflow: 'auto'

  }

  const wrapperStyle = {
    position: 'relative',
    height: props.height ? props.height : '250px'
  }

  const logContent = props.content.map((logObj, line) => {
    const { lvl, msg } = logObj
    console.log(logObj)
    return (
      <tr key={`line_${line}`}>
        <th style={numberStyle} key={`number_line_${line}`}>
          {line + 1}
        </th>
        <th style={lvlStyles[lvl]} key={`level_line_${line}`}>
          {lvlNames[lvl]}
        </th>
        <th style={txtStyles[lvl]} key={`text_line_${line}`}>
          {msg}
        </th>
      </tr>
    )
  })

  const table = (
    <div style={wrapperStyle}>
      <div style={backgroundStyle}>
        <table style={tableStyle}>
          <tbody>
            {logContent}
          </tbody>
        </table>
      </div>
    </div>
  )

  return (
    <>
      {table}
    </>
  )
}

LoggingViewer.propTypes = {
  content: PropTypes.array.isRequired,
  height: PropTypes.bool
}

export default LoggingViewer
