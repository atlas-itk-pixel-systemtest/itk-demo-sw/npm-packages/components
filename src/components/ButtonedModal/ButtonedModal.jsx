import React from 'react'
import {
// Used Patternfly packages
  Button,
  Modal
} from '@patternfly/react-core'
import PropTypes from 'prop-types'

/**
 * ButtonedModal provides a Button, which con click toggles a Modal with custom
 * content.
 *
 * Props (required options marked with *):
 * {
 *  buttonText*: string - Text of toggle button
 *  content*: object - JSX code defining the Modal content
 *  isOpen*: boolean - Is Modal open?
 *  toggleModal*: function - Toggle open state of modal
 *  buttonVariant: string - variant of the button (default: 'primary')
 *  title: string - Titletext of the modal
 *  width: string - Width of the Modal (default: '75%)
 * }
 */

const ButtonedModal = (props) => {
  const handleToggle = props.toggleModal
  return (
    <>
      <Button
        variant={props.buttonVariant || 'primary'}
        onClick={handleToggle}
      >
        {props.buttonText}
      </Button>
      <Modal
        width={props.width || '75%'}
        title={props.title || ''}
        isOpen={props.isOpen}
        onClose={handleToggle}
        aria-label='My Modal'
      >
        {props.content}
      </Modal>
    </>
  )
}

ButtonedModal.propTypes = {
  buttonText: PropTypes.string.isRequired,
  content: PropTypes.object.isRequired,
  isOpen: PropTypes.bool.isRequired,
  toggleModal: PropTypes.func.isRequired,
  buttonVariant: PropTypes.string,
  title: PropTypes.string,
  width: PropTypes.string
}

export default ButtonedModal
