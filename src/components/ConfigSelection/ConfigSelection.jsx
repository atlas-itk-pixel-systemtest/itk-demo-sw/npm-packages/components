import React from 'react'
import {
  Button,
  FileUpload,
  Flex,
  FlexItem,
  Tooltip
} from '@patternfly/react-core'
import { TypeaheadSelectInput } from '../'
import PropTypes from 'prop-types'

/**
 * ConfigSelection component provides the option to select a config either from
 * a running database or by uploading a manual file.
 *
 * Props:
 * {
 *  configData: string - Config payload (='' if config is chosen from DB)
 *  configName: string - Config name (either filename or DB tag)
 *  handleConfigChange: function - Called when a config is uploaded or selected
 *  handleUploadChange: function - Called when (de-)selecting upload option
 *  type: string - Type of config (used in default text)
 *  upload: boolean - Config to be uploaded? (false if selected from DB)
 *  configList: array - List of DB config names to choose from
 *  selectDisabled: boolean - true, if DB-upload should be disabled
 * }
 */

const ConfigSelection = (props) => {
  const select = (
    <TypeaheadSelectInput
      selected={props.configName}
      selectOptions={props.configList ? props.configList : []}
      isOpen={props.isSelectOpen}
      onSelect={props.onSelect}
      onToggle={props.onToggle}
      clearSelection={props.clearSelection}
      placeholderText={`Select ${props.type} config`}
      isInputValuePersisted
      onTypeaheadInputChanged={(name) => props.handleConfigChange('', name)}
      maxheight='450%'
      noValidate
    />
  )

  const upload = (
    <FileUpload
      id='simple-file'
      type='text'
      filenamePlaceholder={`Upload ${props.type} config`}
      value={props.configData}
      filename={props.configName}
      onChange={props.handleConfigChange}
      hideDefaultPreview
      browseButtonText='Upload'
    />
  )

  return (
    <Flex spaceItems={{ default: 'spaceItemsNone' }}>
      <Flex>
        <FlexItem style={{ width: '350px' }}>
          {props.upload || props.selectDisabled ? upload : select}
        </FlexItem>
      </Flex>
      <Tooltip
        content='Config DB is not available'
        trigger={props.selectDisabled ? 'mouseenterfocus' : 'manual'}
        isVisible={false}
      >
        <div>
          <Button
            variant='control'
            onClick={props.handleUploadChange}
            style={{ width: '100px' }}
            isDisabled={props.selectDisabled}
          >
            {props.upload || props.selectDisabled ? 'From DB' : 'Upload'}
          </Button>
        </div>
      </Tooltip>
    </Flex>
  )
}

ConfigSelection.propTypes = {
  clearSelection: PropTypes.func.isRequired,
  configData: PropTypes.string.isRequired,
  configName: PropTypes.string.isRequired,
  handleConfigChange: PropTypes.func.isRequired,
  handleUploadChange: PropTypes.func.isRequired,
  isSelectOpen: PropTypes.bool.isRequired,
  onSelect: PropTypes.func.isRequired,
  onToggle: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
  upload: PropTypes.bool.isRequired,
  configList: PropTypes.array,
  selectDisabled: PropTypes.bool
}

export default ConfigSelection
