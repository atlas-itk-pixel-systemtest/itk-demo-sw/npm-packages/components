import React, { useState, useEffect } from 'react'
import { generalRequest } from '@itk-demo-sw/utility-functions'
import {
  Card as C,
  CardHeader,
  CardTitle,
  CardActions,
  CardBody
} from '@patternfly/react-core'
import PropTypes from 'prop-types'

/**
 * Cards used for the Dashboard. They display the data for a given service.
 * If the service has a defined health endpoint regular request will be send
 * to check its status.
 *
 * Props (required marked with *):
 * {
 *  selected*: boolean - States if the Card should be expanded.
 *  service*: object - All the information of a given service
 *  onClick*: function -  Called when a Card gets clicked
 *  host*: string - Standard url to wich request will be send, if the service has no defined host
 *  advanced*: boolean - States if advanced infos should be shown
 * }
 */

const Card = (props) => {
  const [healthStatus, setHealthStatus] = useState(undefined)
  const keysToShowWhenSelected = ['host', 'hostport']

  const getHealth = (card) => {
    if (!card || !('health' in card) || !('url' in card)) { return }
    generalRequest(`${card.url}${card.health}`).then(
      data => {
        const response = Math.floor(data.status / 100)
        setHealthStatus(response === 2 ? true : response === 4 ? false : undefined)
      }
    ).catch(
      e => {
        setHealthStatus(false)
      }
    )
  }

  useEffect(() => {
    getHealth(props.service)
    const interval = setInterval(() => {
      getHealth(props.service)
    }, 10e3)
    return () => clearInterval(interval)
  })

  const formatKey = (key) => {
    let fkey = props.keyDisplayNames[key]
    if (fkey) return fkey
    const f = key.slice(1)
    const start = key[0].toUpperCase()
    fkey = start + f
    fkey = fkey.split(/_+/).map(str => {
      const f = str.slice(1)
      const start = str[0].toUpperCase()
      return start + f
    }).join(' ')
    let shifted = 1
    const numbers = fkey.matchAll(/[0-9]+/g)
    shifted = 0
    for (const n of numbers) {
      const i = n.index
      const end = fkey.slice(i + shifted)
      const start = fkey.slice(0, i + shifted)
      fkey = start + ' ' + end
      shifted++
    }
    return fkey
  }

  const border = {}
  if (props.color) {
    border.borderColor = props.color
    border.borderWidth = '1px'
  }
  let description = {}
  let title = {}
  if (!props.selected) {
    title = {
      textOverflow: 'ellipsis',
      overflow: 'hidden'
    }
    description = {
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
      overflow: 'hidden',
      maxWidth: '100%'
    }
  }
  return (
    <C
      hasSelectableInput
      style={{ marginBottom: '0px', minHeight: '150px', height: props.selected ? 'auto' : `${100 * props.heightFraction - 5}%`, border: '1px solid #ececec', position: 'relative', ...border }}
      id={props.service.id}
      onClick={() => props.onClick(props.service.id)}
      onSelectableInputChange={() => props.onClick(props.service.id)}
      isSelectableRaised
      isSelected={props.selected}
      key={props.service.id}
    >
      <CardHeader>
        {props.service.icon && <img src={props.service.icon} alt={`${props.service.name} icon`} style={{ maxWidth: '60px' }} />}
        <CardTitle
          style={{
            height: '3em',
            ...title
          }}
        >
          {
            props.service.url
              ? (
                <a
                  href={`${props.service.url}${props.urlExtension || ''}`}
                  target='_blank'
                  rel='noreferrer'
                  onClick={(e) => e.stopPropagation()}
                >
                  {props.service.name}
                </a>)
              : props.service.name
          }
        </CardTitle>
        <CardActions>
          <div style={{ borderRadius: '50%', width: '0.7rem', height: '0.7rem', border: '1px solid black', backgroundColor: ('health' in props.service ? healthStatus === undefined ? 'yellow' : healthStatus ? 'greenyellow' : 'red' : 'lavender') }} />
        </CardActions>
      </CardHeader>
      <CardBody>
        <div>
          <div
            style={{
              float: 'left',
              ...description
            }}
          >
            {props.service.description}
          </div>
          {!props.selected && (
            <>
              <br />
              <div
                style={{
                  fontStyle: 'italic',
                  float: 'left',
                  whiteSpace: 'pre-wrap'
                }}
              >
                {' ' + props.service.host}
              </div>
            </>
          )}
        </div>
        <br />
        {props.selected &&
          <>
            <span>Status: </span>
            <span>
              {(('health' in props.service ? healthStatus === undefined ? 'requesting' : healthStatus ? 'available' : 'unavailable' : 'unknown health endpoint'))}
            </span>
          </>}
        <br />
        {/** Map Keys that are marked for being shown */}
        {props.selected && Object.entries(props.service).filter(([key, _]) => (props.keysToShow ? props.keysToShow.includes(key) : false) || keysToShowWhenSelected.includes(key)).map(([key, value]) => {
          return (
            <>
              <span key={`${key}name`}>{formatKey(key)}: </span>
              <span key={`${key}value`} onClick={(e) => e.stopPropagation()} style={{ userSelect: 'all', cursor: 'text' }}>
                {value}
              </span>
              <br />
            </>
          )
        })}
      </CardBody>
    </C>
  )
}

Card.propTypes = {
  selected: PropTypes.bool.isRequired,
  service: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
  host: PropTypes.string.isRequired,
  keyDisplayNames: PropTypes.object.isRequired,
  heightFraction: PropTypes.number,
  keysToShow: PropTypes.array,
  urlExtension: PropTypes.string,
  color: PropTypes.string
}

export default Card
