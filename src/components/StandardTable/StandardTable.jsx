import React from 'react'
import {
  Bullseye,
  EmptyState,
  EmptyStateIcon,
  Title
} from '@patternfly/react-core'
import {
  Table,
  TableHeader,
  TableBody,
  Thead,
  Tbody,
  Tr,
  Th,
  Td
} from '@patternfly/react-table'
import {
  SearchIcon
} from '@patternfly/react-icons'
import PropTypes from 'prop-types'

/**
 * The StandardTable provides a regular table, for a given set of rows and
 * columns.
 *
 * Columns need to be given as an array of objects, of the following form:
 * [{ title: <col-name> }, ...]
 * Rows need to be given as a 2D array of entries, of the following form:
 * [[<Row1Col1>, <Row1Col2>, ...], [<Row2Col1>, <Row2Col2>, ...], ...]
 * Entries can be strings or JSX elements. The dimensions of individual rows
 * needs to match that of the 'columns' array.
 *
 * Props (required marked with *):
 * {
 *  columns*: array - List of columns (as specified above)
 *  rows*: array - 2D Array of rows (as sepcified above)
 *  emptyTable: object - JSX element which is shown when table rows are empty
 *  isHoverable: boolean - Animate row on mouse hover
 *  onRowClick: function - Called when row is clicked
 *  selectedRows: array - Inlcudes IDs of selected rows
 *  variant: string - Variant of the table (default: 'compact')
 * }
 */

const StandardTable = (props) => {
  const emptyTable = (
    <>
      <Table
        variant={props.variant || 'compact'}
        aria-label='standard table'
        cells={props.columns}
        rows={[]}
      >
        <TableHeader />
        <TableBody />
      </Table>
      {
        props.emptyTable
          ? props.emptyTable
          : (
            <Bullseye>
              <EmptyState>
                <EmptyStateIcon icon={SearchIcon} />
                <Title headingLevel='h5' size='lg'>
                  No content to be depicted
                </Title>
              </EmptyState>
            </Bullseye>
          )
      }
    </>
  )
  return (
    props.rows.length === 0
      ? emptyTable
      : (
        <Table
          variant={props.variant || 'compact'}
          aria-label='standard table'
        >
          <Thead noWrap>
            <Tr key='title-row'>
              {props.columns.map((col, colIndex) =>
                <Th key={`title_${col.title}`}>
                  {col.title}
                </Th>
              )}
            </Tr>
          </Thead>
          <Tbody>
            {
              props.rows.map((row, rowIndex) => {
                return (
                  <Tr
                    key={rowIndex}
                    onRowClick={(event) => {
                      if (props.onRowClick) props.onRowClick(row)
                    }}
                    isHoverable={props.isHoverable}
                    isRowSelected={
                      props.selectedRows && props.selectedRows.includes(rowIndex)
                    }
                  >
                    {row.map((cell, cellIndex) => {
                      return (
                        <Td
                          key={`${rowIndex}_${cellIndex}`}
                          dataLabel={props.columns[cellIndex]}
                        >
                          {cell}
                        </Td>
                      )
                    })}
                  </Tr>
                )
              })
            }
          </Tbody>
        </Table>
      )
  )
}

StandardTable.propTypes = {
  columns: PropTypes.array.isRequired,
  rows: PropTypes.array.isRequired,
  emptyTable: PropTypes.any,
  isHoverable: PropTypes.bool,
  onRowClick: PropTypes.func,
  selectedRows: PropTypes.array,
  variant: PropTypes.string
}

export default StandardTable
