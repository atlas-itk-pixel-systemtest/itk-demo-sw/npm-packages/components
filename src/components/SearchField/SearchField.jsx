import React from 'react'
import {
  InputGroup,
  Button,
  ButtonVariant,
  TextInput
} from '@patternfly/react-core'
import {
  SearchIcon
} from '@patternfly/react-icons'
import PropTypes from 'prop-types'

/**
 * The SearchField component provides a TextField with a search button, which
 * in combination can be used for text searches.
 *
 * Props:
 * {
 *  handleChange: function - Called when TextInput value changes
 *  handleSearch: function - Called on search button or 'Enter' press
 *  value: string - Value of the TextField
 *  name: string - Default string shown, when value is undefined
 *  width: string - Width of the Search field (default '150px')
 * }
 */

const SearchField = (props) => {
  const name = props.name || 'Search'

  const onKeyDown = (event) => {
    if (
      props.value !== '' &&
      props.value !== undefined &&
      (event.key && event.key === 'Enter')
    ) {
      props.handleSearch()
    }
  }

  return (
    <InputGroup>
      <TextInput
        name={'SearchInput' + name}
        id={'SearchInput' + name}
        aria-label={'SearchInput' + name}
        type='search'
        onChange={props.handleChange}
        onKeyDown={onKeyDown}
        value={props.value !== undefined ? props.value : name}
        style={{
          color: props.value === undefined ? 'rgb(0,0,0,0.5)' : 'black',
          width: props.width || '150px'
        }}
        onFocus={() => {
          if (props.value === undefined) props.handleChange('')
        }}
        onBlur={() => { if (props.value === '') props.handleChange(undefined) }}
      />
      <Button
        variant={ButtonVariant.control}
        aria-label={'search button for search input ' + name}
        onClick={props.handleSearch}
      >
        <SearchIcon />
      </Button>
    </InputGroup>
  )
}

SearchField.propTypes = {
  handleChange: PropTypes.func.isRequired,
  handleSearch: PropTypes.func.isRequired,
  value: PropTypes.string,
  name: PropTypes.string,
  width: PropTypes.string
}

export default SearchField
