import React from 'react'
import {
  DataList
} from '@patternfly/react-core'
// PropTypes
import PropTypes from 'prop-types'

const createTree = (id, tree, expandedMap, toggleComp, metaData, createLevel, createLowestLevel, onCompSelect, isTitleButton) => {
  if (tree === null) return createLowestLevel(id, metaData, onCompSelect)
  const content = Object.entries(tree).map(([id, children]) => {
    return createTree(id, children, expandedMap, toggleComp, metaData, createLevel, createLowestLevel, onCompSelect, isTitleButton)
  })
  return createLevel(id, metaData, content, expandedMap, toggleComp, onCompSelect, isTitleButton)
}

const Tree = (props) => {
  if (props.expandedMap == null) return null
  const id = Object.keys(props.tree)[0]
  if (id == null) return null
  const tree = createTree(id, props.tree[id], props.expandedMap, props.toggleComp, props.metaData, props.createLevel, props.createLowestLevel, props.onCompSelect, props.isTitleButton)

  return (
    <DataList
      style={{
        width: '650px'
      }}
      isCompact
    >
      {tree}
    </DataList>
  )
}

Tree.propTypes = {
  tree: PropTypes.object.isRequired,
  expandedMap: PropTypes.object.isRequired,
  toggleComp: PropTypes.func.isRequired,
  metaData: PropTypes.object.isRequired,
  createLevel: PropTypes.func.isRequired,
  createLowestLevel: PropTypes.func.isRequired,
  onCompSelect: PropTypes.func.isRequired,
  isTitleButton: PropTypes.bool.isRequired
}

export default Tree
