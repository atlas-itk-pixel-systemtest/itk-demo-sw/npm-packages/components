# components

Common React components for GUIs of itk-demo microservices. 

## How to change components and publish to private registry

A quick quide to how individual components can be adapted and others can be added. 
**Make sure that any changes to existing components are backwards compatible!**

### Checking out the git repository
The following explanation assumes that you cloned the repository into
```
mylocal-itk-demo-sw/npm-packages/components/
```
And test the changes with the local project living in
```
mylocal-itk-demo-sw/itk-demo-template/
```
### Building
After you have coded your changes you can build them using:
```
cd mylocal-itk-demo-sw/npm-packages/components/
npm i
npm run build
```
The built React components are then stored in the `build/`directory in the project's root folder. 
Before you push your changes to gitlab or publish them to the package registry, you can import them into a local project. This allows you to test your changes and avoid messing up the public registry. Install the locally built components from your project as follows: 
```
cd mylocal-itk-demo-sw/itk-demo-template/ui
npm i ../../npm-packages/components/
npm link ../../npm-packages/components/node_modules/react/
```
*Note: The linking of react is necessary to avoid having multiple versions of React running, which would result in errors. Best reinstall all packages in the testing project after you have validated your components.*
You can access the local components via `import { ... } from '@itk_demo_sw/components'`. To check that you are actually using your local changes, look at the local `node_modules` folder in your testing project. If the `@itk_demo_sw/components` entry is a symbolic link to your local installation of this repository, everything worked fine.
Once you are sure that your changes work as expected you have to commit your changes to git. Then you can continue publishing them to the package registry. 

### Publishing
**Note: This still has to be done manually!** Eventually gitlab CI should automatically integrate changes to components. For now this has not yet been implemented. 

To publish first change the version of your package. You have three choices of version incrementing: major, minor and patch. Use 'minor' whenever you add a new hook or make larger changes to an existing one. Use 'patch' whenever you make small changes to an existing hook or fix a bug. Never use 'major'.
The incrementing can by done via:
```
npm version [minor | patch]
```
Once you have done this, publish your changes to the package registry using:
```
npm publish
```
Now all your changes should be available in the registry.